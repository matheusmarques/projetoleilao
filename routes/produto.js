var Produto = require('../models/produto.js');
var Usuario = require('../models/usuario.js');

exports.incluir = function(req, res){
	var cpfCnpj = req.body.cpfCnpj;

	vendedorBusca = Usuario.find({cpfCnpj: cpfCnpj}, function(err, vendedorBusca){
		if(err){
			res.send(err)
		}
		else{
			if(vendedorBusca == null){
				return res.send("Não foi possível localizar o usuario de CPF/CNPJ:'"+cpfCnpj+"'. ");
			}

			produto = new Produto({
				nome          : req.body.nome,
				valor         : req.body.valor,
				tipo_pagamento: req.body.tipo_pagamento,
				categoria     : req.body.categoria,
				valor_inicial : req.body.valor_inicial,
				valor_final   : req.body.valor_final,
				tempo_leilao  : req.body.tempo_leilao,
				data_inicial  : new Date(),
				vendedor      : vendedorBusca
			});

            produto.save(function (err) {
				if (err)
					res.send(err);
				else
					console.log("Produto gravado com sucesso");
			});

			res.json(produto);
		}	
	});
}

exports.buscar = function(req, res){
	Produto.find(function(err, produtos) {
		if (err){
			res.send(err)
		}else{
		res.json(produtos);	
		}
	});
};

exports.buscarPorCpfCnpj = function(req, res){
    console.info(req.params.cpfCnpj)
    var cpfCnpj = req.params.cpfCnpj;
    Produto.find({'vendedor.cpfCnpj':cpfCnpj},function(err, produto) {
        if (err)
            res.send(err)
        else{
            res.json(produto);
        }
    });

};
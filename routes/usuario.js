var Usuario = require('../models/usuario.js');

exports.incluir = function(req, res){
	usuario = new Usuario({
    	nome     : req.body.nome,		
		cpfCnpj  : req.body.cpf_cnpj,
		endereco : req.body.endereco,
		email    : req.body.email,
		telefone : req.body.telefone
    });

    usuario.save(function (err) {
    	if (err)
      		res.send(err);
    	else
    		console.log("Usuario criado com sucesso");
  	});

    res.json(usuario);
}


exports.buscar = function(req, res){
   Usuario.find(function(err, usuarios){
       if(err)
            res.send(err)
       else
            res.json(usuarios)
   });
};
exports.buscar = function(req, res){
  	var tipos_de_pagamento = [
  								{id:1,nome:'Dinheiro'},
  								{id:2,nome:'Master Card - 1x'},
  								{id:3,nome:'Master Card - 2x'},
  								{id:4,nome:'Master Card - 3x'},
  								{id:5,nome:'Visa - 1x'},
  								{id:6,nome:'Visa - 2x'},
  								{id:7,nome:'Visa - 3x'}
  							];
	res.json(tipos_de_pagamento);  							
};
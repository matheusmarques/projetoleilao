var Servico = require('../models/servico.js');
var Usuario = require('../models/usuario.js');

exports.incluir = function(req, res){
  var cpfCnpj = req.body.cpfCnpj;


  vendedorBusca = Usuario.find({cpfCnpj:cpfCnpj}, function(err, vendedorBusca){
    if(err)
      res.send(err)
    else{
      if(vendedorBusca == null){
        return res.send("Não foi possível localizar o usuario de CPF/CNPJ:'"+cpfCnpj+"'. ");
      }

      servico = new Servico({
        categoria      : req.body.categoria,
        valor          : req.body.valor,
        nome           : req.body.nome,
        tipo_pagamento : req.body.tipo_pagamento,    
        inicio_servico : req.body.inicio_servico,
        duracao_dias   : req.body.duracao_dias,
        periodo        : req.body.periodo,
        vendedor       : vendedorBusca
      });

      servico.save(function (err) {
        if (err)
          res.send(err);
        else
          console.log("Serviço criado com sucesso!");
      });
      res.json(servico);    
    }
  });
};

exports.buscar = function(req, res){
  Servico.find(function(err, servicos) {
    if (err)
      res.send(err)
    res.json(servicos);
  });
};

exports.buscarPorCpfCnpj = function(req, res){
    console.info(req.params.cpfCnpj)
    var cpfCnpj = req.params.cpfCnpj;
    Servico.find({'vendedor.cpfCnpj':cpfCnpj},function(err, servicos) {
        if (err)
            res.send(err)
        else{
            res.json(servicos);
        }
    });

};

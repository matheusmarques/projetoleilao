var Lance   = require('../models/lance.js');
var Produto = require('../models/produto.js');
var Usuario = require('../models/usuario.js');
var mongoose = require ('mongoose');
var ObjectId = mongoose.Types.ObjectId;

exports.incluir = function(req, res){
	var id_produto  = req.body.id_produto;
	var valor_lance = req.body.valor;
	var cpfCnpj   = req.body.cpfCnpj;

    compradorLance = Usuario.find({cpfCnpj: cpfCnpj}, function(err, compradorLance){
        if(err){
            res.send(err);
        }
        else{
            Produto.findById(id_produto, function(err, produto){
                if(err){
                    res.send(err);
                } else {
                    if(produto == null){
                        return res.send("Não foi possível localizar o produto de id:'"+id_produto+"'. ");
                    }

                    if(compradorLance == null){
                        return res.send("Não foi possível localizar o comprador de id:'"+compradorLance.cpfCnpj+"'. ");
                    }

                    if(valor_lance < produto.valor_inicial){
                        return res.send("o valor do lance deve ser maior que o valor inicial do leilão (R$"+produto.valor_inicial+").");
                    }
                    Lance.findOne({"produto._id": ObjectId(id_produto)}).sort('-valor')
                        .exec(function(err, lance){
                            console.log(lance);
                            if((lance != null) && (valor_lance <= lance.valor)){
                                console.log("Lance não autorizado");
                                return res.send("o lance atual não pode ser menor/igual a um lance já realizado. <br> "+
                                    "Valor do lance atual: R$"+lance.valor);
                            }
                            else{
                                lance = new Lance({
                                    data_hora : new Date(),
                                    valor     : valor_lance,
                                    produto   : produto,
                                    comprador : compradorLance
                                });

                                lance.save(function(err){
                                    if(err)
                                        res.send(err);
                                    else
                                        console.log("Lance criado");
                                    console.log(lance);
                                });
                                res.json(lance);
                            }
                        });
                }
            });
        }
    });
};
exports.buscar = function(req, res){
    var cpfCnpj = req.params.cpfCnpj;
    Lance.find({"comprador.cpfCnpj": cpfCnpj},function(err, produto) {
        if (err){
            res.send(err)
        }
        else{
            res.json(produto);
            console.info("CPF/CNPJ "+cpfCnpj);
        }
    });

};

exports.todosLances = function(req, res){
    Lance.find(function(err, produto) {
        if (err){
            res.send(err);
        }
        else{
            res.json(produto);
        }
    });
};

exports.buscarProduto = function(req, res){
    var id_produto = req.params.id_produto;
    Lance.findOne({"produto._id": ObjectId(id_produto)}).sort('-valor')
        .exec(function(err, produto){
            if (err){
                res.send(err)
            }
            else{
                res.json(produto);
                console.info("Produto "+id_produto);
            }
        });
};
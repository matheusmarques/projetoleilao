var Compra = require('../models/compra.js');
var Servico = require('../models/servico.js');
var Usuario = require('../models/usuario.js');
var mongoose = require ('mongoose');
var ObjectId = mongoose.Types.ObjectId;

exports.incluir = function(req, res){//Funciona apenas para a compra de serviços
	id_servico = req.body.id_servico;
    cpfCnpjUsuario = req.body.cpfCnpj;

	Usuario.find({cpfCnpj: cpfCnpjUsuario}, function(err, compradorBusca){
		if(err) 
			res.send(err);
		else{
			if(compradorBusca == null){
				console.log("Não foi possível localizar o usuario de cpf/cnpj:'"+cpfCnpjUsuario+"'. ");
				return res.send("Não foi possível localizar o usuario de cpf/cnpj:'"+cpfCnpjUsuario+"'. ");
			}
			servicoBusca = Servico.findById(id_servico, function(err, servicoBusca){
				if(servicoBusca == null){
					console.log("Não foi possível localizar o serviço de id:'"+id_servico+"'. ");
					return res.send("Não foi possível localizar o serviço de id:'"+id_servico+"'. ");
				}
				Compra.findOne({"itemDeVenda._id": ObjectId(id_servico)}).
				exec(function(err, compraBusca){
					if(compraBusca != null){
						console.log("O serviço de id '"+id_servico+"' já foi comprado pelo usuario '"+compraBusca.comprador.nome+"'.");						
						res.json("O serviço de id '"+id_servico+"' já foi comprado pelo usuario '"+compraBusca.comprador.nome+"'.");
					}else{
						compra = new Compra({
							data            : new Date(),
							valor           : servicoBusca.valor,
							tipoDePagamento : req.body.tipo_pagamento,
							tipoEntrega     : req.body.tipo_entrega,
							itemDeVenda     : servicoBusca,
							comprador       : compradorBusca
						});

						compra.save(function(err){
							if(err)
								res.send(err);
							else
								console.log("Compra criada");
							console.log(compra);
						});

						Servico.remove({ '_id': id_servico }, function (err) {
							if (err) 
								res.send(err);
							else
								console.log("Removido o serviço da coleção de serviços disponíveis.");
						});	

						res.json(compra);
					}
				});	
});
}
});
};

exports.buscar = function(req, res){
    console.info(req.params.cpfCnpj)
	var cpfCnpj = req.params.cpfCnpj;
	Compra.find({'comprador.cpfCnpj':cpfCnpj},function(err, compras) {
		if (err)
			res.send(err)
		else{
            res.json(compras);
        }
	});
	
};

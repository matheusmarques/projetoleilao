var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Produto', {
	nome           : String,
	valor          : Number,
	tipo_pagamento : Number,
	categoria      : String,
	valor_inicial  : Number,
	valor_final    : Number,
	tempo_leilao   : Date, 
	data_inicial   : Date,
	vendedor       : Schema.Types.Mixed
});
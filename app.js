var express  = require('express');
var routes   = require('./routes');
var tipos_de_pagamento = require('./routes/tipo_de_pagamento');
var categoria = require('./routes/categoria');
var produto  = require('./routes/produto');
var servico  = require('./routes/servico');
var usuario  = require('./routes/usuario');
var lance    = require('./routes/lance');
var compra   = require('./routes/compra');
var http     = require('http');
var path     = require('path');
var mongoose = require('mongoose');
var database = require('./config/database');
var maiorLanceJob = require('./jobs/MaiorLanceJob');

var app = express();
//app
mongoose.connect(database.url);

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

app.get('/produtos',    produto.buscar);
app.get('/produto/:cpfCnpj', produto.buscarPorCpfCnpj);
app.post('/produto',   produto.incluir);

app.post('/lance', lance.incluir);
app.get('/lance/:cpfCnpj', lance.buscar);
app.get('/lance/compra/:id_produto', lance.buscarProduto);

app.get('/servicos',    servico.buscar);
app.get('/servico/:cpfCnpj', servico.buscarPorCpfCnpj);
app.post('/servico',   servico.incluir);

app.get('/tipos_de_pagamento', tipos_de_pagamento.buscar);

app.get('/categorias', categoria.listar);

app.post('/usuario', usuario.incluir);
app.get('/usuarios', usuario.buscar);

app.post('/compra', compra.incluir);
app.get('/compra/:cpfCnpj', compra.buscar);

//Job
/*maiorLanceJob.start();*/

//Testes
app.get('/teste/lances', lance.todosLances);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
